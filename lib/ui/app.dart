import 'package:flutter/material.dart';
import 'package:stopwatch/ui/stopwatch_screen.dart';
import 'login_screen.dart';

class StopwatchApp extends StatelessWidget {
  const StopwatchApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Stopwatch',
      initialRoute: '/login',
      routes: {
        '/login': (context) => LoginScreen(),
        '/stopwatch': (context) => StopwatchScreen(),
      },
    );
  }
}
