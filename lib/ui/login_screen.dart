import 'package:flutter/material.dart';
import 'package:stopwatch/ui/stopwatch_screen.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return LoginScreenState();
  }
}

class LoginScreenState extends State<LoginScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Login')),
      body: loginBody(),
    );
  }

  Widget loginBody() {
    return Container(
      margin: const EdgeInsets.all(10),
      child: Form(
        child: Column(
          children: [
            emailField(),
            passwordField(),
            Container(margin: const EdgeInsets.all(10)),
            loginButton(),
          ],
        ),
      ),
    );
  }

  Widget emailField() {
    return TextFormField(
      keyboardType: TextInputType.emailAddress,
      decoration: const InputDecoration(
        icon: Icon(Icons.email),
        labelText: 'Email address',
      ),
    );
  }

  Widget passwordField() {
    return TextFormField(
      obscureText: true,
      decoration: const InputDecoration(
        icon: Icon(Icons.password),
        labelText: 'Password',
      ),
    );
  }

  Widget loginButton() {
    return ElevatedButton(
      onPressed: () {
        Navigator.pushReplacementNamed(context, '/stopwatch');
      },
      child: const Text('Login')
    );
  }
}
