import 'dart:async';

import 'package:flutter/material.dart';

class StopwatchScreen extends StatefulWidget {
  const StopwatchScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return StopwatchScreenState();
  }
}

class StopwatchScreenState extends State<StopwatchScreen> {
  late Timer timer;
  int milliseconds = 0;

  bool isPaused = false;
  bool isTicking = false;


  final scrollController = ScrollController();
  final List<int> laps = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: stopwatchAppBar(),
      body: Column(
        children: [buildCounter(), Expanded(child: buildLapDisplay())],
      ),
    );
  }

  Widget buildLapDisplay() {
    return Scrollbar(
      child: ListView.separated(
        padding: const EdgeInsets.all(8),
        controller: scrollController,
        itemCount: laps.length,
        itemBuilder: (BuildContext context, int index) {
          return ListTile(
            contentPadding: const EdgeInsets.symmetric(horizontal: 50),
            title: Text('Lap ${index + 1}'),
            trailing: Text('${laps[index] / 1000}'),
          );
        },
        separatorBuilder: (BuildContext context, int index) => const Divider(),
      ),
    );
  }

  Widget buildCounter() {
    return Container(
      child: Column(
        children: [
          Text('${milliseconds / 1000}',
              style: const TextStyle(
                fontSize: 50,
              )),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: controlButtons(),
          ),
          const Divider(
            height: 20,
            // thickness: 5,
            indent: 20,
            endIndent: 20,
            color: Colors.black,
          ),
        ],
      ),
    );
  }

  List<Widget> controlButtons() {
    List<Widget> controlButtonsList = [
      ElevatedButton(
        child: Text(
          isTicking ? 'Stop' : 'Start',
          style: const TextStyle(color: Colors.white),
        ),
        style: ButtonStyle(
          backgroundColor: isTicking
              ? MaterialStateProperty.all<Color>(Colors.red)
              : MaterialStateProperty.all<Color>(Colors.green),
        ),
        onPressed: isTicking ? stopTimer : startTimer,
      ),
      Container(width: 10),
      ElevatedButton(
          onPressed: isTicking ? getLap : null, child: const Text('Lap')),
    ];
    if (isTicking) {
      controlButtonsList.insertAll(0, [
        ElevatedButton(
          onPressed: isPaused ? resumeTimer : pauseTimer,
          child: Text(
            isPaused ? 'Resume' : 'Pause',
            style: const TextStyle(color: Colors.white),
          ),
          style: ButtonStyle(
            backgroundColor: isPaused
                ? MaterialStateProperty.all<Color>(Colors.pink)
                : MaterialStateProperty.all<Color>(Colors.orange),
          ),
        ),
        Container(width: 10),
      ]);
    }
    return controlButtonsList;
  }

  void resumeTimer() {
    startTimer();
    setState(() {
      isPaused = false;
      isTicking = true;
    });
  }

  void getLap() {
    scrollController.animateTo(
      70 * laps.length.toDouble(), // 70 is itemHeight sth
      duration: Duration(milliseconds: 500),
      curve: Curves.easeIn,
    );

    laps.add(milliseconds);
    setState(() {});
  }

  void pauseTimer() {
    timer.cancel();
    setState(() {
      // isTicking = false;
      isPaused = true;
    });
  }

  void stopTimer() {
    timer.cancel();
    setState(() {
      isTicking = false;
      isPaused = false;
      showDialog(
          context: context,
          builder: (context) {
            return AlertDialog(
                title: const Text('Run completed!'),
                content: Text('Total run time: ${milliseconds/1000}s \n Number of laps: ${laps.length}'),
                actions: [
                  TextButton(
                      child: const Text('Close'),
                      onPressed: () {
                        milliseconds = 0;
                        laps.clear();
                        Navigator.of(context).pop();
                      })
                ]);
          });
    });

  }

  void _onTick(Timer timer) {
    milliseconds += 100;
    // print('ms = $milliseconds');
    setState(() {});
  }

  void startTimer() {
    timer = Timer.periodic(const Duration(milliseconds: 100), _onTick);
    setState(() {
      isTicking = true;
      isPaused = false;
    });
  }

  AppBar stopwatchAppBar() {
    return AppBar(
      title: const Text('Stopwatch'),
      actions: [
        IconButton(
          onPressed: () {
            Navigator.pushReplacementNamed(context, '/login');
          },
          icon: const Icon(Icons.home),
        )
      ],
    );
  }
}
